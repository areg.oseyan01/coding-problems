#include <iostream>
#include <unordered_map>

struct ListNode 
{
  int data;
  struct ListNode* next;
};

struct ListNode* newNode(int data)
{
  ListNode* temp = new ListNode;
  temp->data = data;
  temp->next = nullptr;
  return temp;
}

struct ListNode* detectCycle(ListNode* head)
{
  std::unordered_map<ListNode*, int> umap;
  while(head)
  {
    if (umap[head] != 0) { return head; }
    umap[head]++;
    head = head->next;
  }
  return nullptr;
}

int main()
{
  struct ListNode* head1 = newNode(3);
  head1->next = newNode(2);
  head1->next->next = newNode(0);
  head1->next->next->next = newNode(4);
  head1->next->next->next->next = head1->next;

  struct ListNode* res = detectCycle(head1);
  std::cout << res->data << std::endl;

  return {};
}
