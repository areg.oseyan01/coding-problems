#include <iostream>
#include <string>

// O(n * n)
bool isUnique(const std::string& st)
{
  for (int i = 0; i < st.size(); ++i)
  {
    for (int j = i + 1; j < st.size(); ++j)
    {
      if (st[i] == st[j]) { return false; }
    }
  }
  return true;
}

// O(n)
bool isUniqueGood(const std::string& st)
{
  bool helper[127];
  for (int i = 0; i < 127; ++i) { helper[i] = false; }
  for (auto p : st)
  {
    if (helper[(int)p]) { return false; }
    else { helper[(int)p] = true; }
  }
  return true;
}

int main()
{
  std::string str = "Hello!!!";
  std::string str1 = "Bay";

  if (isUnique(str)) { std::cout << str << " has all unique characters" << std::endl; }
  else {  std::cout << str << " hasn't all unique characters" << std::endl;  }
  if (isUnique(str1)) { std::cout << str1 << " has all unique characters" << std::endl; }
  else {  std::cout << str1 << " hasn't all unique characters" << std::endl;  }
  std::cout << "********************************************" << std::endl;
  if (isUniqueGood(str)) { std::cout << str << " has all unique characters" << std::endl; }
  else {  std::cout << str << " hasn't all unique characters" << std::endl;  }
  if (isUniqueGood(str1)) { std::cout << str1 << " has all unique characters" << std::endl; }
  else {  std::cout << str1 << " hasn't all unique characters" << std::endl;  }

  return {};
}
