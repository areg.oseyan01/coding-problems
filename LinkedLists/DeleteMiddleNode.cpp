#include <iostream>

struct ListNode 
{
  int data;
  struct ListNode* next;
};

struct ListNode* newNode(int data)
{
  ListNode* temp = new ListNode;
  temp->data = data;
  temp->next = nullptr;
  return temp;
}

void deleteMiddleNode(ListNode* head) 
{
  if (!head) { return; }
	if (!head->next) 
  {
		delete head;
		return;
	}
	int count = 0;
	ListNode* nd1 = head;
	ListNode* nd2 = head;
	while (nd2 && nd2->next) 
  {
		nd2 = nd2->next->next;
		nd1 = nd1->next;
	}
  nd1->data = nd1->next->data; 
  ListNode* tmp = nd1->next;
  nd1->next = nd1->next->next;
  
  delete tmp;
  nd1 = nullptr;
  tmp = nullptr;
	return;
}

int main()
{
  struct ListNode* head1 = newNode(1);
  head1->next = newNode(9);
  head1->next->next = newNode(1);
  head1->next->next->next = newNode(2);
  head1->next->next->next->next = newNode(4);

  deleteMiddleNode(head1);

  while (head1)
  {
    std::cout << head1->data << " ";
    head1 = head1->next;
  }
  std::cout << std::endl;

  return {};
}
