#include <iostream>
#include <unordered_set>

struct ListNode
{
  int data;
  struct ListNode* next;
};

struct ListNode* newNode(int data)
{
  ListNode* temp = new ListNode;
  temp->data = data;
  temp->next = nullptr;
  return temp;
}

void deleteDups(ListNode* node) 
{
  if (!node || !node->next) { return; }
  std::unordered_set<int> helper;
  ListNode* prev;
  while (node) 
  {
    if(helper.find(node->data) == helper.end()) 
    {
      helper.insert(node->data);
      prev = node;
      node = node->next;
    } 
    else 
    {
      node = node->next;
      prev->next = node;
    }
  }
}

int main()
{
  struct ListNode* head1 = newNode(3);
  head1->next = newNode(2);
  head1->next->next = newNode(2);
  head1->next->next->next = newNode(4);
  head1->next->next->next->next = newNode(4);
  head1->next->next->next->next->next = newNode(4);
  head1->next->next->next->next->next->next = newNode(4);

  deleteDups(head1);
  while (head1)
  {
    std::cout << head1->data << " ";
    head1 = head1->next;
  }
  std::cout << std::endl;

  return {};
}
