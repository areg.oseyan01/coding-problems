#include <iostream>

struct ListNode
{
  int data;
  struct ListNode* next;
};

struct ListNode* newNode(int data)
{
  ListNode* temp = new ListNode;
  temp->data = data;
  temp->next = nullptr;
  return temp;
}

struct ListNode* KthToLast(ListNode* head, int k) 
{
	if (!head || !head->next) { return nullptr; }
  ListNode* help = head;
	while (k) 
  {
		help = help->next;
		k--;
	}
	ListNode* res = head;
	while (help) 
  {
		help = help->next;
		res = res->next;
	}
	return res;
}

int main()
{
  struct ListNode* head1 = newNode(3);
  head1->next = newNode(2);
  head1->next->next = newNode(0);
  head1->next->next->next = newNode(4);
  head1->next->next->next->next = newNode(12);

  struct ListNode* res = KthToLast(head1, 4);
  std::cout << res->data << std::endl;

  return {};
}
