#include <iostream>

struct ListNode 
{
  int data;
  struct ListNode* next;
};

struct ListNode* newNode(int data)
{
  ListNode* temp = new ListNode;
  temp->data = data;
  temp->next = nullptr;
  return temp;
}

struct ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) 
{
  if(headA == nullptr || headB == nullptr)
  {
    return nullptr;
  }
  ListNode* head1 = headA;
  ListNode* head2 = headB;
  while (head1 != head2)
  {
    if (head1 == nullptr) { head1 = headB; }
    else { head1 = head1->next; }
    if (head2 == nullptr) { head2 = headA; }
    else { head2 = head2->next; }
  }
  return head1;
}

int main()
{
  struct ListNode* head1 = newNode(1);
  head1->next = newNode(9);
  head1->next->next = newNode(1);
  head1->next->next->next = newNode(2);
  head1->next->next->next->next = newNode(4);

  struct ListNode* head2 = newNode(3);
  head2->next = head1->next->next->next;
  head2->next->next = head1->next->next->next->next;


  struct ListNode* res = getIntersectionNode(head1, head2);
  std::cout << res->data << std::endl;

  return {};
}
