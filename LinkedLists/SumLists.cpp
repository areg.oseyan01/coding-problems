#include <iostream>
#include <cmath>

struct ListNode 
{
  int data;
  struct ListNode* next;
};

struct ListNode* newNode(int data)
{
  ListNode* temp = new ListNode;
  temp->data = data;
  temp->next = nullptr;
  return temp;
}

struct ListNode* getSum(ListNode* head1, ListNode* head2)
{
  int num1 = 0;
  int num2 = 0;
  int i = 0;
  while (head1)
  {
    num1 += head1->data * std::pow(10, i);
    head1 = head1->next;
    i++;
  }
  i = 0;
  while (head2)
  {
    num2 += head2->data * std::pow(10, i);
    head2 = head2->next;
    i++;
  }
  std::cout << num1 << std::endl;
  std::cout << num2 << std::endl;

  num1 += num2;
  int reversedNumber = 0; 
  while(num1 != 0) 
  {
    reversedNumber = (reversedNumber * 10) + (num1 % 10);
    num1 /= 10;
  }

  struct ListNode* help = newNode(reversedNumber % 10);
  reversedNumber /= 10;
  struct ListNode* res = help;
  while (reversedNumber > 0)
  {
    help->next = newNode(reversedNumber % 10); 
    reversedNumber /= 10;
    help = help->next;
  }
  return res;
}


int main()
{
  struct ListNode* head1 = newNode(7);
  head1->next = newNode(1);
  head1->next->next = newNode(6);

  struct ListNode* head2 = newNode(5);
  head2->next = newNode(9);
  head2->next->next = newNode(2);

  struct ListNode* res = getSum(head1, head2);
  std::cout << "Result is\n";
  while (res)
  {
    std::cout << res->data << " ";
    res = res->next;
  }
  std::cout << std::endl;

  return {};
}
