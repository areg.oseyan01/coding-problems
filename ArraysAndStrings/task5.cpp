#include <iostream>
#include <string>

bool remove(std::string s1, std::string s2)
{
  int count = 0;
  for (int i = 0; i < s1.size(); ++i)
  {
    if (s1[i] != s2[i])
    {
      count++;
      if (count > 2) { return false; }
    }
  }
  return true;
}

bool replace(std::string s1, std::string s2)
{
  for (int i = 0; i < s1.size(); ++i)
  {
    if (s1[i] != s2[i])
    {
      s2[i] = s1[i];
      break;
    }
  }
  return s1 == s2;
}

bool insert(std::string s1, std::string s2)
{
  std::string tmp;
  for (int i = 0; i < s1.size(); ++i)
  {
    if (s1[i] != s2[i])
    {
      tmp += s1[i];
      while (i < s2.size())
      {
        tmp += s2[i];
        i++;
      }
      break;
    }
    tmp += s2[i];
  }
  return s1 == tmp;
}

int main()
{
  std::string str1 = "pale";
  std::string str2 = "bake";

  if (str1.size() < str2.size())
  {
    if (remove(str1, str2)) { std::cout << "Good;)\n"; }
    else { std::cout << "Bad;(\n"; }
  }
  else if (str1.size() > str2.size())
  {
    if (insert(str1, str2)) { std::cout << "Good;)\n"; }
    else { std::cout << "Bad;(\n"; }
  }
  else
  {
    if (replace(str1, str2)) { std::cout << "Good;)\n"; }
    else { std::cout << "Bad;(\n"; }
  }

  return {};
}
