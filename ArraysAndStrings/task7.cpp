#include <iostream>

int main()
{   
  int arr[3][3] = { {21, 22, 23},
                    {31, 32, 33},
                    {41, 42, 43} };

  int n = 3;
  std::cout <<"Source matrix\n";
  for (int i = 0; i < n; ++i)
  {
      for (int j = 0; j < n; ++j) { std::cout << arr[i][j] << " "; }
      std::cout << "\n";
  }

  for (int i = 0; i < n / 2; ++i) 
  {
    for (int j = i; j < n - i - 1; ++j) 
    { 
      int temp = arr[i][j]; 
      arr[i][j] = arr[n - 1 - j][i]; 
      arr[n - 1 - j][i] = arr[n - 1 - i][n - 1 - j]; 
      arr[n - 1 - i][n - 1 - j] = arr[j][n-1-i]; 
      arr[j][n - 1 - i] = temp; 
    } 
  }

  std::cout <<"Matrix after rotating 90 degree\n";
  for (int i = 0; i < n; ++i)
  {
      for (int j = 0; j < n; ++j) { std::cout << arr[i][j] << " "; }
      std::cout << "\n";
  }

  return {};
}
