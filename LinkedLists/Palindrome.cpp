#include <iostream>
#include <stack>

struct ListNode 
{
  int data;
  struct ListNode* next;
};

struct ListNode* newNode(int data)
{
  ListNode* temp = new ListNode;
  temp->data = data;
  temp->next = nullptr;
  return temp;
}

bool isPalindrome(ListNode* head) 
{
  std::stack<int> s;
  ListNode* temp = head;
  while (temp != nullptr)
  {   
    s.push(temp->data);
    temp = temp->next;
  }
  ListNode* curr = head;
  while (curr != nullptr)
  {
    if ((curr->data) == s.top()) { s.pop(); }
    else { return false; }
    curr = curr->next;
  }
  if(s.empty() == true) { return true; }
  return false;
}

int main()
{
  struct ListNode* head1 = newNode(1);
  head1->next = newNode(9);
  head1->next->next = newNode(1);
  head1->next->next->next = newNode(9);
  head1->next->next->next->next = newNode(11);

  if (isPalindrome(head1)) { std::cout << "Is palindrome\n"; }
  else { std::cout << "Isn't palindrome\n"; }

  return {};
}
