#include <iostream>
#include <string>
#include <unordered_map>

bool isPermutation(const std::string& s1, const std::string& s2)
{
  std::unordered_map<char, int> helper;
  for (int i = 0; i < s1.size(); ++i) 
  { 
    if (s1[i] >= 'A' && s1[i] <= 'Z') { helper[s1[i] + 32]++; }
    else { helper[s1[i]]++; } 
  }
  for (auto y : s2) { helper[y]--; }
  for (auto z : helper) { if (z.second) { return false; } }
  return true;
}

int main()
{
  std::string st = "Tact Coa";

  std::string st1 = "atco eta";
  std::string st2 = "taco cat";

  if (isPermutation(st, st1))
  {
    std::cout << "Is a permutation" << std::endl;
  }
  else
  {
    std::cout << "Isnt a permutation" << std::endl;
  }

  return {};
}
